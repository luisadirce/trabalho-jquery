$(document).ready(function() {
  
    //load from the server
    $.ajax('https://jquery-19-1.herokuapp.com/destinations.html', {
       method: "POST",
       dataType: 'json',
       contentType: 'application/json',
       cache: false,
       success: function () {
        alert("success");
    },
    })
    .done(function(response){
      console.log(response);
      var html;
      $.each(response, function(index, element){
        html = '<div class="item-box" data-id="'+element.id+'">';
        html += '<div class="item-title">'+element.name+'</div>';
        html += '<p>'+element.description+'</p>';
        html += '<div class="item-price">$'+element.price+'</div>';
        html += '<button>Add to cart</button>';
        html += '<div><a href="#" class="info-link">More info</a></div>';
        html += '<div class="more-info"><p>'+element.more_info+'</p></div>';
        html += '</div>';
  
      $('body').append(html);
      
      });
      alert(result);
    });

    $('body').on('click', '.info-link', function(event) {
      event.preventDefault();
      $(this).closest('.item-box').find('.more-info').slideToggle();
    }); 
    
  });
  